if (annyang) {
    
    function openmenu(e) {
        $("#toggle" + name).prop('checked', true);
    }

    function closemenu(e) {
        $("#toggle" + name).prop('checked', false);
    }
    // Let's define our first command. First the text we expect, and then the function it should call
    var commands = {
        '(go) (to) home': function () {
            window.location = ("/index.html");
        },

        '(go) (to) about me': function () {
            window.location = ("/aboutme.html");
        },

        '(go) (to) pets': function () {
            window.location = ("/pets.html");
        },

        '(go) (to) hobbies': function () {
            window.location = ("/hobbies.html");
        },
        
        '(go) back (arrow)': function() {
            goBack();
        },

        'gaming': function () {
            document.getElementById('game').click();
        },

        'sports': function () {
            document.getElementById('sport').click();
        },

        'movies': function () {
            document.getElementById('movie').click();
        },

        'interests': function () {
            document.getElementById('interest').click();
        },

        '(go) (to) University Life': function () {
            window.location = ("/unilife.html");
        },

        '(go) (to) contact me': function () {
            window.location = ("/contactme.html");
        },


        //Contact me links

        'facebook': function () {
            document.getElementById('fb').click();
        },

        '(insta)gram': function () {
            document.getElementById('ig').click();
        },


        'linked in': function () {
            document.getElementById('ln').click();
        },

        '(phone) (ring) call': function () {
            document.getElementById('call').click();
        },

        'email': function () {
            document.getElementById('email').click();
        },




        'scroll down': function () {
            scrollBy(0, 300);
        },

        'scroll down two': function () {
            scrollBy(0, 600);
        },

        'scroll up': function () {
            scrollBy(0, -300);
        },

        'scroll up two': function () {
            scrollBy(0, -600);
        },

        'back to (the) top': function () {
            window.scrollTo(0, 0);
        },

        'open menu': function () {
            openmenu();
            window.scrollTo(0, 0);
        },

        'close menu': function () {
            closemenu();
            window.scrollTo(0, 0);
        },



    };





    // Add our commands to annyang
    annyang.addCommands(commands);


    // Start listening. You can call this here, or attach this call to an event, button, etc.
    annyang.start();
}
